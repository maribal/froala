<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class RolesController extends Controller
{
    //

    public function check(Request $request) {
        if (!auth()->check()){
            return $this->failure("UnAuthorised");
        }

        $result = [];
        if($request->get('per_slug')) {


            $per_slug = $request->get('per_slug');

            foreach ($per_slug as $permission) {

                /**
                 *
                 * Check for default permissions
                 *
                 */

                $list = ['browse_admin','browse_media','browse_database','browse_bread','browse_compass','browse_hooks'];


                if(count(explode('_',$permission['key'])) > 1 && key_exists($permission['key'],$list)){

                    // slug is pre defined like browse_admin
                    $result[$permission['key']] = auth()->user()->can($permission['key']);


                }else {

                    // all the non-default permissions

                    $dataType = Voyager::model('DataType')->where('slug', '=', $permission['slug'])->first();
                    if ($dataType) {
//                    return $this->success();

                        $result[$permission['key'] . '_' . $permission['slug']] = auth()->user()->can($permission['key'], app($dataType->model_name));
                    } else {
                        $result[$permission['key'] . '_' . $permission['slug']] = false;
                    }
                }
            }

            return $this->success($result);

        }
        return $this->permissionDenied('error code 9047');
    }
}
