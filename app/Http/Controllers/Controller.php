<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use TCG\Voyager\Facades\Voyager;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function permissionDenied($result,$redirect=false){
        return [
            'success'=>false,
            'result'=>null,
            'redirect'=>$redirect,
            'error'=>"Permission Denied"
        ];
    }

    public function success($result,$redirect=false){
        return [
            'success'=>true,
            'result'=>$result,
            'redirect'=>$redirect,
            'error'=>null
        ];
    }

    public function failure($result,$redirect=false,$code=false){
        return [
            'success'=>false,
            'result'=>null,
            'redirect'=>$redirect,
            'error'=>$result,
            'code'=>$code
        ];
    }


    public function can($permission, $slug) {



        if(auth()->check()) {
            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            if ($dataType) {
                return auth()->user()->can($permission, app($dataType->model_name));
            }
            return false;
        }

        return false;

    }
}
