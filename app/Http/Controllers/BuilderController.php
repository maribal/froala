<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BuilderController extends Controller
{

    public function index(Request $request, Page $page){

        return $this->success($page);

    }
    public function build(Request $request) {

        $pageId = $request->get("pageId");
        $page = Page::query()->where('id',$pageId)->first();


        if ($page->html != null) {
            return \App::abort(403);
        }

        if ($this->can('edit','pages')) {
            return view('pages.build');
        }

        return \App::abort(403);
    }

    public function edit(Request $request) {


        $pageId = $request->get("pageId");
        $page = Page::query()->where('id',$pageId)->first();

        if ($page->html == "") {
            return \App::abort(403);
        }

        if ($this->can('edit','pages')) {
            return view('pages.edit');
        }

        return \App::abort(403);

    }

    public function storeBuilder(Request $request, Page $page){

        /* todo check if can edit */

        if ($page->html == '') {

            $content = $request->get('content');

            $pattern = '/<style(.*?)<\/style>/s';
            $newContent = preg_replace($pattern,'',$content);

            $page->html = $newContent;

            $page->save();

            return $this->success('updated', '/page/' . $page->url_slug);
        }
        return $this->permissionDenied('No right','/');
    }

    public function storeEditor(Request $request, Page $page){

        /* todo check if can edit */

        if ($page->html != '') {

            $content = $request->get('content');

            $page->html = $content;

            $page->save();

            return $this->success('updated', '/page/' . $page->url_slug);
        }
        return $this->permissionDenied('No right','/');
    }


    public function storeImage(Page $page, Request $request){


        $file = $request->file('img');
        $name = rand(11111, 999999) . '.' . $file->getClientOriginalExtension();

        $path = public_path()."/storage/pages/".$page->url_slug;
        Storage::makeDirectory($path);
        $request->file('img')->move($path, $name);


        return json_encode(['link'=>'/storage/pages/'.$page->url_slug.'/'.$name]);

    }
}
