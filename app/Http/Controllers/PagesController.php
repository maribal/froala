<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function index(){
        return view('pages.home');
    }

    public function dynamic($slug=""){


        if ($slug) {
            $data = Page::where('url_slug', '=', $slug)->first();


            if (is_null($data))
                \App::abort(404);

        } else {
            $data = Page::query()->orderBy('id', 'asc')->first();
        }
//        $script = "setTimeout(()=>{" . self::getBetween($data->html, "<script>", "</script>") . "}, 200);";
//        $data->html = self::deleteBetween("<script>", "</script>", $data->html);


        $nav = Page::query()->select(['url_slug','url','index','target_blank'])->orderBy('index','asc')->get();



        return view('pages.dynamic', compact(['data','nav','slug']));

    }
}
