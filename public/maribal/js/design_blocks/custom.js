/*!
    * Maribal Pages v1.0.1 (https://github.com/froala-labs/froala-pages#readme)
    * Copyright 2017-2019
    * Licensed under Maribal Pages Terms (http://dcdemo.maribalgroup.com/pages/terms)
    */
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('FroalaPages')) :
	typeof define === 'function' && define.amd ? define(['FroalaPages'], factory) :
	(factory(global.FroalaPages));
}(this, (function (FroalaPages) { 'use strict';

FroalaPages = FroalaPages && FroalaPages.hasOwnProperty('default') ? FroalaPages['default'] : FroalaPages;

FroalaPages.RegisterDesignBlock('Custom', '<section class="fdb-block">\n  <div class="container">\n    <div class="row justify-content-center">\n      <div class="col col-md-8 text-center">\n        <h1>Maribal Design Blocks</h1>\n        <br>\n        <h2>Design your own custom block</h2>\n      </div>\n    </div>\n  </div>\n</section>\n', 'contents/1.jpg');

    FroalaPages.RegisterDesignBlock('Custom', '<section class="fdb-block">\n  <div class="col-fill-left" style="background-image: url({{imagesDir}}/people/5.jpg);">\n  </div>\n\n  <div class="container">\n    <div class="row justify-content-end">\n      <div class="col-12 col-md-5 text-center">\n        <h1>Maribal Blocks</h1>\n        <p class="lead">When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove</p>\n\n        <p class="mt-4"><a href="http://dcdemo.maribalgroup.com">Learn More <i class="fas fa-angle-right"></i></a></p>\n      </div>\n    </div>\n  </div>\n</section>\n', 'contents/26.jpg');

})));
//# sourceMappingURL=custom.js.map
