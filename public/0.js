(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./resources/admin/js/includes/froala_pages.js":
/*!*****************************************************!*\
  !*** ./resources/admin/js/includes/froala_pages.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
  * Froala Pages v1.0.1 (https://github.com/froala-labs/froala-pages#readme)
  * Copyright 2017-2019 
  * Licensed under Froala Pages Terms (https://www.froala.com/pages/terms)
  */
(function (global, factory) {
  ( false ? undefined : _typeof(exports)) === 'object' && typeof module !== 'undefined' ? module.exports = factory() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
})(this, function () {
  'use strict';

  var classCallCheck = function classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  };

  var inherits = function inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + _typeof(superClass));
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  };

  var possibleConstructorReturn = function possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (_typeof(call) === "object" || typeof call === "function") ? call : self;
  }; // Helpers to make changes on the DOM.


  var Helpers = function () {
    function Helpers() {
      classCallCheck(this, Helpers);
    }

    Helpers.createElement = function createElement(html) {
      var el = document.createElement('DIV');
      el.innerHTML = html;
      return el.firstChild;
    };

    Helpers.prepend = function prepend(parent, el) {
      if (!parent.firstChild) {
        parent.append(el);
      } else {
        parent.insertBefore(el, parent.firstChild);
      }
    };

    Helpers.append = function append(parent, el) {
      parent.appendChild(el);
    };

    Helpers.after = function after(node, el) {
      node.parentNode.insertBefore(el, node.nextSibling);
    };

    Helpers.before = function before(node, el) {
      node.parentNode.insertBefore(el, node);
    };

    Helpers.remove = function remove(node) {
      if (node && node.parentNode) {
        node.parentNode.removeChild(node);
      }
    };

    Helpers.offset = function offset(el) {
      var rect = el.getBoundingClientRect();
      return {
        top: rect.top,
        left: rect.left
      };
    };

    Helpers.outerHeight = function outerHeight(el) {
      var withMargin = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var height = el.offsetHeight;

      if (withMargin) {
        var style = getComputedStyle(el);
        height += parseInt(style.marginTop, 10) + parseInt(style.marginBottom, 10);
        return height;
      }

      return height;
    };

    Helpers.outerWidth = function outerWidth(el) {
      var withMargin = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var width = el.offsetWidth;

      if (withMargin) {
        var style = getComputedStyle(el);
        width += parseInt(style.marginLeft, 10) + parseInt(style.marginRight, 10);
        return width;
      }

      return 0;
    };

    Helpers.underscores = function underscores(text) {
      return text.toLowerCase().split(' ').join('_');
    };

    Helpers.scrollTo = function scrollTo(el) {
      el.scrollIntoView({
        behavior: 'smooth',
        block: 'end'
      });
    };

    Helpers.setDownloadCount = function setDownloadCount(scope, count) {
      var downloadBtns = scope.pageToolbar.el.querySelectorAll('.fp-download-btn');
      downloadBtns.forEach(function (btn) {
        if (scope.opts.showDownloadCounter && btn) {
          // update remaining download count for current user
          btn.querySelector('span').innerText = 'Downloads (' + count + ')';
        }
      });
    };

    Helpers.setDownloadBtnsStatus = function setDownloadBtnsStatus(scope, disabledBtn, hide) {
      var downloadBtns = scope.pageToolbar.el.querySelectorAll('.fp-download-btn');
      downloadBtns.forEach(function (btn) {
        // enable or disable download button
        if (!btn) {
          return;
        } // hide download button


        btn.style.display = hide ? 'none' : 'flex';

        if (disabledBtn) {
          btn.setAttribute('disabled', 'disabled');
        } else {
          btn.removeAttribute('disabled');
        }
      });
    };

    return Helpers;
  }(); // Define our internal module class.
  // The Module class is inherited by inernal classes and in the constructor of the module
  // we assign the common used objects for easier access.


  var Module = function Module(page) {
    classCallCheck(this, Module); // Set page instance.

    this.page = page; // Set events.

    this.events = page.events; // Set doc.

    this.doc = page.doc; // Set options.

    this.opts = page.opts; // Call internal init method that works like a constructor.

    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    this.init.apply(this, args);
  }; // A block on the page.


  var Block = function (_M) {
    inherits(Block, _M);

    function Block() {
      classCallCheck(this, Block);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    Block.prototype.init = function init(designBlock) {
      var _this2 = this;

      this.designBlock = designBlock;
      this.id = ++Block.ID; // Create element.

      this.el = Helpers.createElement(designBlock.render({
        imagesDir: this.opts.designsImagesDir
      }));
      this.el.setAttribute('data-block-type', Helpers.underscores(designBlock.blockType));
      this.el.setAttribute('data-id', this.id);
      this.events.on(this.el, 'mouseenter', function () {
        return _this2._mouseEnter();
      });
    }; // Emit block enter event.


    Block.prototype._mouseEnter = function _mouseEnter() {
      this.events.emit('block.mouseenter', this);
    };

    return Block;
  }(Module);

  Block.ID = 0;
  var DRAG_IMAGE_HEIGHT = 50; // A design block is an element from the Designs Panel.

  var DesignBlock = function (_M) {
    inherits(DesignBlock, _M);

    function DesignBlock() {
      classCallCheck(this, DesignBlock);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    DesignBlock.prototype.init = function init(blockType, info) {
      var _this2 = this; // Create design block.


      this.el = document.createElement('IMG');
      this.el.setAttribute('draggable', true);
      this.blockType = blockType;
      this.template = info.template;
      this.image = info.image;
      this.el.setAttribute('src', '' + (this.opts.designsThumbsDir || '') + this.image); // Bind events.

      this.events.on(this.el, 'click', function () {
        return _this2.click();
      });
      this.events.on(this.el, 'dragstart', function (e) {
        return _this2.dragstart(e);
      });
    }; // Click on a block.


    DesignBlock.prototype.click = function click() {
      this.events.emit('designBlock.click', this);
    }; // Start dragging a block.


    DesignBlock.prototype.dragstart = function dragstart(e) {
      // Set dragging placeholder.
      this.dragImage = this.doc.createElement('IMG');
      this.dragImage.src = this.el.src;
      this.dragImage.setAttribute('height', DRAG_IMAGE_HEIGHT);
      this.dragImage.style.position = 'absolute';
      Helpers.append(this.page.body, this.dragImage);
      e.dataTransfer.setDragImage(this.dragImage, 0, 0);
      e.dataTransfer.setData('text/html', null); // Emit event of drag start.

      this.events.emit('designBlock.willDrag', this);
    }; // Render design block.


    DesignBlock.prototype.render = function render(params) {
      return this.template.replace(/{{[a-zA-Z_]*}}/g, function (match) {
        var varName = match.replace(/{|}/g, '').trim();
        return params[varName];
      });
    };

    return DesignBlock;
  }(Module);

  DesignBlock.BLOCKS = {};
  DesignBlock.HEADER = 'Headers';
  DesignBlock.FOOTER = 'Footers'; // Global method to register a new design block.

  DesignBlock.Register = function (type, template, image) {
    if (!DesignBlock.BLOCKS[type]) {
      DesignBlock.BLOCKS[type] = [];
    }

    DesignBlock.BLOCKS[type].push({
      template: template,
      image: image
    });
  }; // Define constants for different views.


  var VIEWS = {
    ADD: 1,
    FULL: 2,
    DESKTOP: 100,
    TABLET_LANDSCAPE: 1024,
    TABLET_PORTRAIT: 768,
    PHONE: 375
  }; // There are 2 type of buttons - both of them work in the same manner only that have different callbacks:
  //   1. on block toolbar
  //   2. on page toolabr

  var Button = function (_M) {
    inherits(Button, _M);

    function Button() {
      classCallCheck(this, Button);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    Button.prototype.init = function init(name) {
      var _this2 = this; // Get button details.


      this.props = Button._[name];

      if (!this.props) {
        throw new Error('A button with the name ' + name + ' could not be found');
      } // Render button.


      this._render(name); // Bind button events.


      this.events.on(this.el, 'click', function () {
        return _this2._click();
      });
      this.events.listen('button.refresh', function () {
        return _this2._refresh();
      }); // Call button callback after build.

      if (this.props.afterBuild) {
        this.props.afterBuild.apply(this);
      }
    }; // Refresh button state.


    Button.prototype._refresh = function _refresh() {
      if (this.props.refresh) {
        this.props.refresh.apply(this);
      }
    }; // Render button.


    Button.prototype._render = function _render(name) {
      this.el = this.doc.createElement('BUTTON');
      this.el.innerHTML = this.props.icon.template;
      this.el.classList.add('fp-btn', 'fp-' + name + '-btn', 'fr-btn-type-' + this.props.icon.type);
      this.el.setAttribute('title', this.props.title);
    }; // Click button callback.


    Button.prototype._click = function _click() {
      this.props.callback.apply(this);
      this.events.emit('button.refresh');
    };

    return Button;
  }(Module); // Register button.


  Button.Register = function (name, props) {
    if (!Button._) {
      Button._ = {};
    }

    Button._[name] = props;
  }; // Define layout button.


  Button.Register('layout', {
    title: 'Layout',
    icon: {
      type: 'svg',
      template: '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n                <path d="M0 0h24v24H0z" fill="none"/>\n                <path d="M2 21h19v-3H2v3zM20 8H3c-.55 0-1 .45-1 1v6c0 .55.45 1 1 1h17c.55 0 1-.45 1-1V9c0-.55-.45-1-1-1zM2 3v3h19V3H2z"/>\n              </svg>'
    },
    callback: function callback() {
      this.page.setView(VIEWS.ADD);
      this.page.designsPanel.show();
    },
    refresh: function refresh() {
      this.el.classList.toggle('fp-active', this.page.activeView === VIEWS.ADD);
    }
  }); // Define block move up butotn.

  Button.Register('moveUp', {
    title: 'Move Up',
    icon: {
      type: 'svg',
      template: '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n            <path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z"/>\n            <path d="M0 0h24v24H0z" fill="none"/>\n          </svg>'
    },
    callback: function callback() {
      var block = this.page.activeBlock;
      var prevBlock = block.el.previousSibling;

      while (prevBlock && (!prevBlock.classList || !prevBlock.classList.contains('fdb-block'))) {
        prevBlock = prevBlock.previousSibling;
      }

      window.a = prevBlock;
      window.b = block.el;

      if (prevBlock) {
        Helpers.before(prevBlock, block.el);
      }

      if (this.page.activeView === VIEWS.FULL) {
        Helpers.scrollTo(block.el);
      }

      this.page.blockToolbar.hide();
      this.page.blockToolbar.refreshPosition();
    },
    refresh: function refresh() {
      if (this.page.activeBlock) {
        this.el.classList.toggle('fp-hidden', [DesignBlock.HEADER, DesignBlock.FOOTER].indexOf(this.page.activeBlock.designBlock.blockType) >= 0);
        var block = this.page.activeBlock;
        var prevBlock = block.el.previousSibling;

        while (prevBlock && (!prevBlock.classList || !prevBlock.classList.contains('fdb-block'))) {
          prevBlock = prevBlock.previousSibling;
        }

        if (!prevBlock) {
          this.el.classList.add('fp-disabled');
          this.el.setAttribute('disabled', 'disabled');
        } else {
          this.el.classList.remove('fp-disabled');
          this.el.removeAttribute('disabled');
        }
      }
    }
  }); // Define block remove button.

  Button.Register('remove', {
    title: 'Remove',
    icon: {
      type: 'svg',
      template: '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n              <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/>\n              <path d="M0 0h24v24H0z" fill="none"/>\n          </svg>'
    },
    callback: function callback() {
      if (this.page.activeBlock) {
        if (confirm('Are you sure you want to remove this design block?')) {
          var block = this.page.activeBlock;
          this.page.blockToolbar.hide();
          Helpers.remove(block.el);
          this.page.refreshBlocks();
        }
      }
    },
    afterBuild: function afterBuild() {
      this.el.classList.add('fp-remove-btn');
    }
  }); // Define page edit button.

  Button.Register('edit', {
    title: 'Edit',
    icon: {
      type: 'svg',
      template: '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n          <path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/>\n          <path d="M0 0h24v24H0z" fill="none"/>\n    </svg>'
    },
    callback: function callback() {
      this.page.setView(VIEWS.FULL);
      this.page.designsPanel.hide();
      this.page.editor.enable();
    },
    refresh: function refresh() {
      this.el.classList.toggle('fp-active', this.page.activeView === VIEWS.FULL);
    }
  }); // Defined page download button.

  Button.Register('download', {
    title: 'Download',
    icon: {
      type: 'html',
      template: '\n    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n        <path d="M19 9h-4V3H9v6H5l7 7 7-7zM5 18v2h14v-2H5z"/>\n        <path d="M0 0h24v24H0z" fill="none"/>\n    </svg>\n    <span>Download</span>'
    },
    callback: function callback() {
      // Download templates only if selected
      var noBlocks = !pages.iframeBody.querySelector('[data-block-type]');
      var btn = pages.pageToolbar.el.querySelector('.fp-download-btn');

      if (!noBlocks) {
        // Download saved HTML.
        pages.getHTML().then(function (html) {
          var blob = new Blob([html], {
            type: 'text/html'
          });
          var url = window.URL.createObjectURL(blob);
          var a = document.createElement('a');
          a.href = url;
          a.download = 'froala_design_blocks_website.html';
          a.click();
        }, function (errorMsg) {
          console.log(errorMsg);
        });
      } else {
        btn.setAttribute('disabled', 'disabled');
      }
    }
  }); // Define desktop mode.

  Button.Register('desktop', {
    title: 'Desktop',
    icon: {
      type: 'svg',
      template: '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n                <path d="M0 0h24v24H0z" fill="none"/>\n                <path d="M21 2H3c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h7l-2 3v1h8v-1l-2-3h7c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 12H3V4h18v10z"/>\n              </svg>'
    },
    callback: function callback() {
      this.page.screenSize = VIEWS.DESKTOP;
      this.page.iframe.style.width = null;
      this.page.el.classList.remove('fp-view-small');
      this.page.refreshIframeSize();
    },
    refresh: function refresh() {
      this.el.classList.toggle('fp-active', this.page.screenSize === VIEWS.DESKTOP);
    },
    afterBuild: function afterBuild() {
      this.page.screenSize = VIEWS.DESKTOP;
    }
  }); // Define tablet landscape mode.

  Button.Register('tablet_landscape', {
    title: 'Tablet Landscape',
    icon: {
      type: 'svg',
      template: '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n    <path d="M0 0h24v24H0z" fill="none"/>\n    <path d="M21 4H3c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h18c1.1 0 1.99-.9 1.99-2L23 6c0-1.1-.9-2-2-2zm-2 14H5V6h14v12z"/>\n              </svg>'
    },
    callback: function callback() {
      this.page.screenSize = VIEWS.TABLET_LANDSCAPE;
      this.page.iframe.style.width = VIEWS.TABLET_LANDSCAPE + 'px';
      this.page.el.classList.add('fp-view-small');
      this.page.refreshIframeSize();
    },
    refresh: function refresh() {
      this.el.classList.toggle('fp-active', this.page.screenSize === VIEWS.TABLET_LANDSCAPE);
    }
  }); // Define tablet portrait mode.

  Button.Register('tablet_portrait', {
    title: 'Tablet Portrait',
    icon: {
      type: 'svg',
      template: '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n                  <path d="M18.5 0h-14C3.12 0 2 1.12 2 2.5v19C2 22.88 3.12 24 4.5 24h14c1.38 0 2.5-1.12 2.5-2.5v-19C21 1.12 19.88 0 18.5 0zm-7 23c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zm7.5-4H4V3h15v16z"/>\n              </svg>'
    },
    callback: function callback() {
      this.page.screenSize = VIEWS.TABLET_PORTRAIT;
      this.page.iframe.style.width = VIEWS.TABLET_PORTRAIT + 'px';
      this.page.el.classList.add('fp-view-small');
      this.page.refreshIframeSize();
    },
    refresh: function refresh() {
      this.el.classList.toggle('fp-active', this.page.screenSize === VIEWS.TABLET_PORTRAIT);
    }
  }); // Define phone mode.

  Button.Register('phone', {
    title: 'Phone',
    icon: {
      type: 'svg',
      template: '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n                <path d="M15.5 1h-8C6.12 1 5 2.12 5 3.5v17C5 21.88 6.12 23 7.5 23h8c1.38 0 2.5-1.12 2.5-2.5v-17C18 2.12 16.88 1 15.5 1zm-4 21c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zm4.5-4H7V4h9v14z"/>\n                <path d="M0 0h24v24H0z" fill="none"/>\n              </svg>'
    },
    callback: function callback() {
      this.page.screenSize = VIEWS.PHONE;
      this.page.iframe.style.width = VIEWS.PHONE + 'px';
      this.page.el.classList.add('fp-view-small');
      this.page.refreshIframeSize();
    },
    refresh: function refresh() {
      this.el.classList.toggle('fp-active', this.page.screenSize === VIEWS.PHONE);
    }
  });
  var LEAVE_TIMEOUT = 50; // Toolbar on a block.

  var BlockToolbar = function (_M) {
    inherits(BlockToolbar, _M);

    function BlockToolbar() {
      classCallCheck(this, BlockToolbar);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    BlockToolbar.prototype.init = function init() {
      // DOM.
      this.el = this.doc.createElement('DIV');
      this.el.classList.add('fp-block-toolbar');
      Helpers.append(this.page.body, this.el); // Buttons.

      this._addButtons(); // Events.


      this._initEvents();
    };

    BlockToolbar.prototype._initEvents = function _initEvents() {
      var _this2 = this;

      this.events.listen('block.mouseenter', function (block) {
        return _this2.showForBlock(block);
      });
      this.events.on(this.page.iframeDoc, 'mouseleave', function () {
        return _this2._doLeave();
      });
      this.events.on(this.el, 'mouseenter', function () {
        return clearTimeout(_this2.leaveTimeout);
      });
      this.events.on(this.el, 'mouseleave', function () {
        return _this2._doLeave();
      });
      this.events.on(this.page.iframeDoc, 'scroll', function () {
        return _this2.refreshPosition();
      });
      this.events.on(this.page.el, 'scroll', function () {
        return _this2.refreshPosition();
      });
    }; // Mouse goes outside of the page.


    BlockToolbar.prototype._doLeave = function _doLeave() {
      var _this3 = this;

      this.leaveTimeout = setTimeout(function () {
        return _this3.hide();
      }, LEAVE_TIMEOUT);
    }; // Add block buttons.


    BlockToolbar.prototype._addButtons = function _addButtons() {
      var _this4 = this;

      this.opts.blockButtons.forEach(function (btnName) {
        var btn = new Button(_this4.page, btnName);
        Helpers.append(_this4.el, btn.el);
      });
    }; // Refresh potion of the buttons on the current block.


    BlockToolbar.prototype.refreshPosition = function refreshPosition() {
      if (this.isVisible()) {
        this.showForBlock(this.page.activeBlock);
      }

      this.events.emit('button.refresh');
    }; // Show buttons toolbar on a specific block.


    BlockToolbar.prototype.showForBlock = function showForBlock(block) {
      clearTimeout(this.leaveTimeout);
      var blockOffset = Helpers.offset(block.el);
      this.page.activeBlock = block;
      var activeEl = this.page.iframeBody.querySelector('.fp-active');

      if (activeEl) {
        activeEl.classList.remove('fp-active');
      }

      block.el.classList.add('fp-active');
      this.show(blockOffset.left, blockOffset.top);
    }; // Show buttons toolbar at a specific position.


    BlockToolbar.prototype.show = function show(left, top) {
      this.el.classList.add('fp-visible');
      this.el.style.left = left + Helpers.offset(this.page.iframe).left + 'px';
      this.el.style.top = Math.max(0, top) / (this.page.activeView === VIEWS.ADD ? 4 : 1) + Helpers.offset(this.page.iframe).top + 'px';
      this.events.emit('button.refresh');
    }; // Hide buttons toolbar.


    BlockToolbar.prototype.hide = function hide() {
      this.el.classList.remove('fp-visible');
    }; // Check if toolbar is visible.


    BlockToolbar.prototype.isVisible = function isVisible() {
      return this.el.classList.contains('fp-visible');
    };

    return BlockToolbar;
  }(Module); // Define constants for different views.


  var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var nolicense = 'FE2bnB-7A24yC-21uakG3B2kI1zzqsoE5A1G4J4C10B10A7hievkH1C10C3C3B2D4F3C3I-8bA-21fdxD-11A-9F3G2wukvvnhxI4I1tdzwE4djnogD5hiczjbG2A7C5uA2B-9eA-7C-7E3B3tijosD1A20A21srqB5nF-10ejj1lA11lE-13C-11D1cursdC3ihrD-8tpxB8A8qwA-9icD1B8ykohdh1hH3C6C7clA2C4G4haB11xsqqmB1E4I4EBCC1smqB-7D3hhmf1bpfzF-7B-22D6A2vvtB-7A26J-7C-21A-16kfplqkbA2C9B6OMG2F1D1I1C8uD-17e1dywnJ4B3xwsE-11uD4fnlwB5voA-9mE2H3C3A2C-13vF3G3A2frfrscA4B1EypC-22ptF6F1WmhlebfzooG3naC-21iE4D-11uD2aA-62rA3c1htA-9qC-7A2Pc1efwG4F4RA-65lB8bxA4bjwpI-7C-21mA3tnF2ioiC-21uE-13lioeG4eC10jnkldxiA3A5D9qA3C3C1pI-8j1A8==';
  var expired = 'lB3bnD-13A4yD-8uakB4A3kE1zzqsoB8C2A5F6A4D4I4hievkE1A4G2I2B1A7A4D5C-11bB-8fdxA-21B-16B4A2wukvvnhxC5F1tdzwA12djnogH3hiczjbE3A3G3uI2B-21eB-16E-13E5G2tijosC2A4D4srqG3nD-17ejf1lH5lJ-7C-22A1cursdI2ihrE-13tpxB4E4qwB-16icA1B3ykohde1hF4A2B3clA4C7E6haA4xsqqmD2E6D5EBCA2smqD-13D5hhmj1bpfzB-11F-10D3I2vvtC-16E6F-11C-9A-8kfplqkbD1H4E3OMA2C1A1E1G4uH-8j1dywnE5G2xwsB-21uB6fnlwD3voB-16mE3F4B1B1A-7vC5B4E4frfrscC10C2EypA-8ptB16B1GA-13tltblA12idA-16tF4g1vC2fB-22qA3PewxvzB2ME-13xerD3G4TicfB-11lF4yickxD4ezvD2yJ-7tB-16wiB-9B2A4C2I-7A1A1A4gmsA1==';
  var connError = 'DC1e1lpG4H-8A-63sgiD4D4uE1xG-7wqmE5C2D5H5G4J4B9jwA-13tiH2G4I2A4B3B8B3A4zdC-7dzfE-13E-13F5A2uwextlnE-11C4E1jzxuC4bhlaiF2joexhA-8E3I3A2sC4gaoH-9A-7H4B1kB-16yzvnfA9A30A8uC-16E6E2G2zsA1nacE-13cD4C2I2SPQC8mF-11chB2zzcnpbpliiA5A6ddbhE2mqrytfzcyI-8A3C3D3Ya1G4A3E2C2C1gmvvkipB2I3naalF-10B3tI-8zaC2dqcD-8E4A5J3C8oxF2C5C4pdtb1mB-16A2A4SobnB-21zD5A4RgyvtxB2kC-13zF-10lD4sgrqltbH-9D3cexhC-11aueD2pB-22D-17xrF-10mstrC6B1OldcpaC3eomjaC3obj1aurfzH-9H-8A11xjA2nieE1C-7xahlA6hdrbzA13B4B10mB3D3C5txdB7==';
  var connSupport = 'MD5igyH3bfjxpD5A2bD2suxxfC6D6F2C1C1G1A1qnlD-11bA2G3F1A1C1D3C5D3cH-9fJ-7cqvtE6C1A-8I-8lomwwaE1F4smqC-16D6maghB-7I3qvzaseC1B3D3zD2njfldA2A32tdbcagmC2J3B5B-21wD4D6E5clB2uxtvhD5D1G1XYFB2vfG-10qF3cmjeymygi1I-7C8B4C-13B-11B-11sD3zxypA-13oqjneB1A10C7E1F1G3C1D1C1D2hdC-13F-11G-10pkD3H2wzxB-11rA4A-7ssjB4shxfG1A28A5B8xqE4A4A3yoH-7wtfB4B2JtH-8ywqA2E7NqreujjjbD5pmgI-7C8D-17E-13aawG1D2CxpwdA-9G2ytriD-16B-13tD5qvtuiuB-13A4kD-8G3B-7F-7C-21d1D-13f1gTsdxyuA-13A2C-9rsA30A9c1D2D1C4aoqA11==';
  var baseUrl = 'noB-13D-11zxH2J4B11xgfB1ufzwB-22yA2ytqB2C-7oiB5tD3G4ugi1mzA7==';
  var RD = 'rA1E1F1C4B8C7B5E1E5C4=='; // Tests to be done:
  // foo.localhost, foo.edu | no key - ok
  // foo.com | no key - unlicensed
  // foo.com | key for foo.com - ok
  // foo.com | expired key for foo.com - expired

  var unlicensedMessage = nolicense;

  var domain = function () {
    var i = 0;
    var domain = document.domain;
    var p = domain.split('.');
    var s = '_gd' + new Date().getTime();

    while (i < p.length - 1 && document.cookie.indexOf(s + '=' + s) == -1) {
      domain = p.slice(0 - 1 - ++i).join('.');
      document.cookie = s + '=' + s + ';domain=' + domain + ';';
    }

    document.cookie = s + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;domain=' + domain + ';';
    return (domain || '').replace(/(^\.*)|(\.*$)/g, '');
  }();

  var Data = function (_M) {
    inherits(Data, _M);

    function Data() {
      classCallCheck(this, Data);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    Data.prototype.init = function init() {
      var me = this;
      var ls = me.page.opts.key;
      var pageToolbar = me.page.pageToolbar;

      var pt = me._decrypt(me._foo('ziRA1E3B9pA5B-11D-11xg1A3ZB5D1D4B-11ED2EG2pdeoC1clIH4wB-22yQD5uF4YE3E3A9==')); // Assume unlicensed.


      pageToolbar.ul = false; // Assume not expired.

      var isExpired = false; // No user id.

      var usrId = 0; // Check key.

      if (ls) {
        // Get info for keys.
        var info = me._parse(ls); // License string.


        var st = info[2]; // Wildcard.

        var xOem = me._decrypt(me._foo(me._decrypt('LGnD1KNZf1CPBYCAZB-8F3UDSLLSG1VFf1A3C2=='))); // OEM.
        // Domain.
        // TODO: Allowed domains by default if any using _isAllowedSubdomain().


        if (st === xOem || st.indexOf(domain, st.length - domain.length) >= 0) {
          // Check if expired.
          if (me._isExpired(info[1]) && (domain || '').length > 0) {
            isExpired = true;
            unlicensedMessage = expired;
            usrId = info[0] || -1;
          } // Is OK.
          else {
              // make request on initial load and update the download count of current user
              me._updateDownloadCount(me);

              pageToolbar.ul = false;
            }
        }
      } // Unlicensed.


      if (pageToolbar.ul === true) {
        // Pixel tracking.
        var img = new Image();

        me._add(unlicensedMessage); // Make a request over to us to track expired.


        if (isExpired) {
          img.src = me._foo(me._decrypt(pt)) + 'e=' + usrId;
        } // Make a request over to us to track unlicensed.
        else {
            img.src = me._foo(me._decrypt(pt)) + 'u';
          }
      }

      if (navigator.connection) {
        navigator.connection.addEventListener('change', function () {
          me._testConnection(me);
        });
      }
    }; // Handle download counts of current user


    Data.prototype._updateDownloadCount = function _updateDownloadCount(me) {
      var countApi = me._decrypt(me._foo(baseUrl)) + 'get_count';
      me.makeRequest(countApi, 'POST', checkDownloadCount);

      function checkDownloadCount(responseText) {
        var res = responseText && JSON.parse(responseText);

        if (res.available_quantity <= 0) {
          res.available_quantity = 0;
          window.open('https://www.froala.com/pages/pricing');
        }

        Helpers.setDownloadCount(me.page, res.available_quantity);
      }
    }; // test internet connection


    Data.prototype._testConnection = function _testConnection(me) {
      var api = me._decrypt(me._foo(baseUrl)) + 'keep_alive';
      me.makeRequest(api, 'GET');
    }; // remove network error message from view


    Data.prototype._remove = function _remove() {
      var errMsg = document.getElementsByClassName('fp-connectin-error');

      if (errMsg.length) {
        for (var i = 0; i < errMsg.length; i++) {
          errMsg[i].remove();
        } // Display downloads button only if blocks are selected


        var noBlocks = !this.page.iframeBody.querySelector('[data-block-type]');
        Helpers.setDownloadBtnsStatus(this.page, noBlocks, noBlocks);
      }
    }; // Add unlicensed message.


    Data.prototype._add = function _add(message, cls) {
      var errorMessage = this._decrypt(this._foo(message));

      var msgDiv = Helpers.createElement(errorMessage);
      cls = cls ? cls : '';

      if (msgDiv) {
        msgDiv.setAttribute('class', 'fp-license-error ' + cls);
        Helpers.prepend(this.page.body, msgDiv);
        Helpers.setDownloadBtnsStatus(this.page, true, true);
      }
    };

    Data.prototype._decrypt = function _decrypt(str) {
      if (!str) {
        return str;
      }

      var decrypted = '';

      var _bar = this._foo('charCodeAt');

      var _car = this._foo('fromCharCode');

      var key = chars.indexOf(str[0]);

      for (var i = 1; i < str.length - 2; i++) {
        var ks = this._sumDigits(++key);

        var charCode = str[_bar](i); // Find out how many times we added the sum of key digits.


        var t = '';

        while (/[0-9-]/.test(str[i + 1])) {
          t += str[++i];
        }

        t = parseInt(t, 10) || 0;
        charCode = this._fromRange(charCode, ks, t);
        charCode ^= key - 1 & 31;
        decrypted += String[_car](charCode);
      }

      return decrypted;
    };

    Data.prototype._isAllowedSubdomain = function _isAllowedSubdomain(st) {
      var localhost = this._decrypt('9qqG-7amjlwq==');

      var domains = [localhost];

      for (var i = 0; i < domains.length; i++) {
        // IE 11 polyfill
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith
        if (!String.prototype.endsWith) {
          String.prototype.endsWith = function (search, this_len) {
            if (this_len === undefined || this_len > this.length) {
              this_len = this.length;
            }

            return this.substring(this_len - search.length, this_len) === search;
          };
        }

        if (st.endsWith(domains[i])) {
          return true;
        }
      }

      return false;
    };

    Data.prototype._parse = function _parse(key) {
      var info = (this._decrypt(key) || '').split('|');

      if (info.length === 4 && info[0] === 'PagesV1') {
        return [info[1], info[3], info[2]];
      }

      return [null, null, ''];
    };

    Data.prototype._isExpired = function _isExpired(date) {
      // no date.
      if (date === null) {
        return true;
      } // date expired.


      if (new Date(date) < new Date(this._decrypt(RD))) {
        return true;
      }

      return false;
    };

    Data.prototype._sumDigits = function _sumDigits(number) {
      var str = number.toString();
      var sum = 0;

      for (var i = 0; i < str.length; i++) {
        sum += parseInt(str.charAt(i), 10);
      }

      if (sum > 10) {
        return sum % 9 + 1;
      }

      return sum;
    };

    Data.prototype._fromRange = function _fromRange(charCode, ks, i) {
      // Get how many times we should substract.
      var p = Math.abs(i);

      while (p-- > 0) {
        charCode -= ks;
      } // Add 123 if the index is negative.


      if (i < 0) {
        charCode += 123;
      }

      return charCode;
    };

    Data.prototype._foo = function _foo(x) {
      return x;
    };

    Data._handleHtml = function _handleHtml(me, resolve, reject) {
      var p = me.page;
      var donwloadsCountURL = me._decrypt(me._foo(baseUrl)) + 'get_count';
      var updateDownloadCount = me._decrypt(me._foo(baseUrl)) + 'update_count';
      Helpers.setDownloadBtnsStatus(p, true);
      me.makeRequest(donwloadsCountURL, 'POST', serverRespone, resolve, reject);

      function serverRespone(response, resolve, reject) {
        var res = response && JSON.parse(response); // make sure we have received available_quantity from server

        if (res && res.hasOwnProperty('available_quantity')) {
          if (res.available_quantity > 0) {
            var updateCountResponse = function updateCountResponse() {
              // Destroy editor instances so that we remove editor UX.
              p.editor.disable(); // Create a HTML structure.

              var html = '<!DOCTYPE html>\n                <html>\n                <head>\n                ' + p._getHeadHTML() + '\n                </head>\n                <body>\n                ' + p._getBodyHTML() + '\n                </body>\n                </html>\n                '; // Init editor back.

              p.editor.enable();
              res.available_quantity -= 1;
              resolve(html);
              Helpers.setDownloadCount(p, res.available_quantity);
              Helpers.setDownloadBtnsStatus(p, false);
            }; // process updateCount request


            me.makeRequest(updateDownloadCount, 'POST', updateCountResponse, resolve, reject);
          } else {
            Helpers.setDownloadBtnsStatus(p, true);
            window.open('https://www.froala.com/pages/pricing');
            reject('Please upgrade your license!');
          }
        } else {
          Helpers.setDownloadBtnsStatus(p, false);
          reject('Error, ' + res.message);
        }
      }
    };

    Data.prototype.makeRequest = function makeRequest(url, methodType, callBack, resolve, reject) {
      var me = this;
      var request = new XMLHttpRequest();
      var params = methodType === 'POST' ? JSON.stringify({
        key: me.opts.key
      }) : {};

      request.onreadystatechange = function () {
        if (request.readyState === 4) {
          me._remove();

          if (request.status === 200) {
            callBack && callBack(request.responseText, resolve, reject);
          } else if (request.responseText) {
            me._add(connSupport);

            reject && reject(request.responseText);
          }
        }
      };

      request.onerror = function () {
        me._add(connError, 'fp-connectin-error');

        reject && reject('Error, Please retry!');
      };

      request.open(methodType, url);
      request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
      request.send(params);
    };

    return Data;
  }(Module); // CSS to be injected in the iframe.


  var STYLESHEET = '\nbody, html {\n  margin: 0;\n  height: 100%;\n}\n\nbody.no-block {\n  flex-flow: column;\n  overflow-x: hidden;\n  display: flex;\n}\n\nbody {\n  background: #DEDEDE;\n}\n\n.fp-no-block {\n  align-items: center;\n  justify-content: center;\n  font-size: 20px;\n  flex-direction: column;\n  cursor: pointer;\n  color: #444;\n  display: none;\n  font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;\n  text-align: center;\n  background: #FFF;\n  transition: margin 0.25s linear 0.25s;\n}\n\n.fp-no-block:hover {\n  background: #EFEFEF;\n}\n\n.fp-no-block.fp-visible {\n  display: flex;\n}\n\n.fp-no-sections {\n  flex: 2;\n}\n\n.fp-no-block p {\n  margin: 0;\n}\n\n.fp-no-sections p + p {\n  font-size: 16px;\n  margin-top: 5px;\n}\n\n.fdb-block {\n  box-shadow: 0;\n  margin-bottom: 0;\n  transition: margin 0.25s linear 0.25s;\n}\n\n.fp-active {\n  box-shadow: 0px 0px 20px rgba(0,0,0,0.14), 0 0px 6px rgba(0,0,0,0.16);\n  -moz-box-shadow: 0px 0px 20px rgba(0,0,0,0.14), 0 0px 6px rgba(0,0,0,0.16);\n  -webkit-box-shadow: 0px 0px 20px rgba(0,0,0,0.14), 0 0px 6px rgba(0,0,0,0.16);\n  z-index: 9999;\n}\n\nbody.fp-add-view [data-block-type],\nbody.fp-add-view .fp-no-block {\n  margin-bottom: 20px;\n}\n\nbody.fp-add-view [data-block-type] {\n  user-select: none;\n  position: relative;\n}\n\nbody.fp-add-view section[data-block-type] {\n  cursor: move;\n}\n\nbody.fp-add-view [data-block-type]:after {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  display: block;\n  z-index: 10000;\n  content: "";\n}\n\n.fp-drop-placeholder {\n  height: 200px;\n  width: 100%;\n  border: solid 10px  #0098f7;\n  background: #FFF;\n  margin-bottom: 20px;\n}\n\n.fp-dragging {\n  display: none;\n}\n\n.fr-popup {\n  z-index: 10000 !important;\n}\n'; // DEFAULTS.

  var options = {
    // Path to load design blocks thumbs.
    designsThumbsDir: '/public/maribal/screenshots/',
    // Path to load images for a design block.
    designsImagesDir: '/public/maribal/dist/imgs/',
    // Default block type to be selected.
    designsDefaultBlocks: 'Contents',
    // CSS to be injected.
    designStylesheets: ['https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', 'https://cdn.jsdelivr.net/gh/froala/design-blocks@master/dist/css/froala_blocks.min.css', 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', 'https://cdn.jsdelivr.net/npm/froala-editor/css/froala_editor.pkgd.min.css', 'https://cdn.jsdelivr.net/npm/froala-editor/css/froala_style.min.css'],
    // JS to be injected.
    designJavascripts: ['https://code.jquery.com/jquery-3.3.1.slim.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', 'https://cdn.jsdelivr.net/npm/froala-editor/js/froala_editor.pkgd.min.js', 'https://use.fontawesome.com/releases/v5.5.0/js/all.js'],
    // Application default CSS.
    appStyle: STYLESHEET,
    // Buttons for a block.
    blockButtons: ['moveUp', 'remove'],
    // licensing key for pages
    key: '',
    // display/hide download counter
    showDownloadCounter: true,
    // Page toolbar left buttons.
    pageLeftButtons: [['edit', 'layout'], ['desktop', 'tablet_landscape', 'tablet_portrait', 'phone']],
    // Page toolbar center buttons.
    pageCenterButtons: ['<div style="display: flex;"><span style="margin-top: 5px;">Powered by </span><a href="https://dcdemo.maribalgroup.com" style="display: inline-block; height: 20px; margin-left: 10px; line-height: 30px; color: #c2c2ff; text-decoration: none" title="MaribalGroup" target="_blank">MaribalGroup.com</a></div>'],
    // Page toolbar right buttons.
    pageRightButtons: [['download']],
    // Custom options for the editor.
    editorOptions: {
      iconsTemplate: 'font_awesome_5'
    }
  }; // Designs panel.

  var Panel = function (_M) {
    inherits(Panel, _M);

    function Panel() {
      classCallCheck(this, Panel);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    Panel.prototype.init = function init(title, content) {
      this.el = this.doc.createElement('DIV');
      this.el.setAttribute('class', 'fp-panel');

      this._addTitle(title);

      this._addContent(content);

      Helpers.append(this.page.container, this.el);
      this.render();
    };

    Panel.prototype._addContent = function _addContent(content) {
      var body = this.doc.createElement('DIV');
      body.setAttribute('class', 'fp-panel-body');

      if (content) {
        body.innerHTML = content;
      }

      Helpers.append(this.el, body);
      this.body = body;
    };

    Panel.prototype._addTitle = function _addTitle(title) {
      var head = this.doc.createElement('DIV');
      head.setAttribute('class', 'fp-panel-head');

      if (title) {
        var headTitle = this.doc.createElement('H2');
        headTitle.innerHTML = title;
        Helpers.append(head, headTitle);
      }

      Helpers.append(this.el, head);
      this.head = head;
    };

    Panel.prototype.show = function show() {
      this.el.classList.add('fp-visible');
    };

    Panel.prototype.hide = function hide() {
      this.el.classList.remove('fp-visible');
    };

    Panel.prototype.isVisible = function isVisible() {
      return this.el.classList.contains('fp-visible');
    };

    return Panel;
  }(Module); // Designs Panel is the zone where we can choose a design block to use.


  var DesignsPanel = function (_Panel) {
    inherits(DesignsPanel, _Panel);

    function DesignsPanel() {
      classCallCheck(this, DesignsPanel);
      return possibleConstructorReturn(this, _Panel.apply(this, arguments));
    }

    DesignsPanel.prototype.render = function render() {
      this._initFilters();

      this._initPanel();

      this.loadedDesigns = {};
    }; // Filter the current shown design blocks.


    DesignsPanel.prototype._initFilters = function _initFilters() {
      var _this2 = this; // Add filter.


      var filter = document.createElement('DIV');
      filter.classList.add('fp-panel-filter');
      Helpers.append(this.head, filter); // Render the curent selected design blocks.

      var _loop = function _loop(blockType) {
        if (Object.prototype.hasOwnProperty.call(DesignBlock.BLOCKS, blockType)) {
          var btn = _this2.doc.createElement('A');

          btn.setAttribute('data-block-type', Helpers.underscores(blockType));
          btn.innerHTML = blockType;
          Helpers.append(filter, btn);

          _this2.events.on(btn, 'click', function () {
            return _this2.setActiveBlock(blockType);
          });
        }
      };

      for (var blockType in DesignBlock.BLOCKS) {
        _loop(blockType);
      } // Store filter.


      this.filter = filter;
    };

    DesignsPanel.prototype._initPanel = function _initPanel() {
      // Create panel for displaying contents.
      var panel = document.createElement('DIV');
      panel.classList.add('fp-panel-panel');
      this.panel = panel;
      Helpers.append(this.body, panel);
    }; // Refresh the filters based on the current active block type.


    DesignsPanel.prototype._refreshFilters = function _refreshFilters(blockType) {
      // Hide all panels.
      var activeBlock = this.panel.querySelector('.fp-panel-blocks-wrapper.fp-visible');

      if (activeBlock) {
        activeBlock.classList.remove('fp-visible');
        this.filter.querySelector('a.fp-active').classList.remove('fp-active');
      }

      this.filter.querySelector('a[data-block-type="' + Helpers.underscores(blockType) + '"]').classList.add('fp-active');
    }; // Refresh designs based on the current active block type.


    DesignsPanel.prototype._refreshDesigns = function _refreshDesigns(blockType) {
      var wp = void 0; // Designs for the current filter weren't added yet.

      if (!this.loadedDesigns[Helpers.underscores(blockType)]) {
        wp = document.createElement('DIV');
        wp.setAttribute('data-block-type', Helpers.underscores(blockType));
        wp.classList.add('fp-panel-blocks-wrapper');
        Helpers.append(this.panel, wp);

        for (var i = 1; i <= DesignBlock.BLOCKS[blockType].length; i++) {
          var designBlock = new DesignBlock(this, blockType, DesignBlock.BLOCKS[blockType][i - 1]);
          Helpers.append(wp, designBlock.el);
        }

        this.loadedDesigns[Helpers.underscores(blockType)] = true;
      } else {
        wp = this.panel.querySelector('div.fp-panel-blocks-wrapper[data-block-type="' + Helpers.underscores(blockType) + '"]');
      } // Toggle classes to show the right designs.


      wp.classList.add('fp-visible');
    }; // Set the current active block type.


    DesignsPanel.prototype.setActiveBlock = function setActiveBlock() {
      var blockType = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Sections'; // Set default block type.

      if (blockType === 'Sections') {
        blockType = this.opts.designsDefaultBlocks;
      } // Refresh filters.


      this._refreshFilters(blockType); // Refresh shown designs.


      this._refreshDesigns(blockType);
    };

    return DesignsPanel;
  }(Panel); // Init editor on the current blocks.


  var Editor = function (_M) {
    inherits(Editor, _M);

    function Editor() {
      classCallCheck(this, Editor);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    Editor.prototype.init = function init() {// Init placeholder.
    };

    Editor.prototype._preventTypingInLinks = function _preventTypingInLinks(editor) {
      editor.events.on('keydown', function (kde) {
        if (editor.link.get()) {
          kde.preventDefault();
          return false;
        }

        return true;
      }, true);
    };

    Editor.prototype.enable = function enable() {
      var _this2 = this;

      this.FroalaEditor = this.page.iframeDoc.defaultView.FroalaEditor;

      this._defineLinkButtons(); // scope reference used in initialized method


      var me = this; // Enable editor on all blocks.

      this.page.iframeBody.querySelectorAll('[data-block-type]').forEach(function (block) {
        // Enable editor on cols.
        block.querySelectorAll('[class*="col"]').forEach(function (col) {
          // No link inside it.
          if (!col.querySelector('.nav-link')) {
            _this2.froalaEditor = new _this2.FroalaEditor('[class*="col"]', Object.assign(_this2.opts.editorOptions, {
              toolbarInline: true,
              pluginsEnabled: ['image', 'link', 'align', 'colors', 'emoticons', 'paragraphFormat'],
              toolbarButtons: ['bold', 'italic', 'textColor', 'backgroundColor', 'paragraphFormat', 'align', '-', 'emoticons', 'insertLink', 'insertImage', 'undo', 'redo'],
              zIndex: 10000,
              initOnClick: true,
              colorsHEXInput: false,
              keepFormatOnDelete: true,
              toolbarVisibleWithoutSelection: true,
              imageEditButtons: ['imageReplace'],
              linkEditButtons: ['linkEdit', 'linkButton', 'linkRemove'],
              events: {
                initialized: function initialized() {
                  // this is the editor instance.
                  me._preventTypingInLinks(this);
                }
              }
            }));
          }
        }); // Init editor on header links.

        block.querySelectorAll('header .nav-link, header nav a.btn').forEach(function (link) {
          _this2.froalaEditor = new _this2.FroalaEditor('header .nav-link, header nav a.btn', Object.assign(_this2.opts.editorOptions, {
            linkEditButtons: ['linkEdit', 'linkButton', 'linkAdd', 'linkDestroy'],
            linkInsertButtons: [],
            events: {
              initialized: function initialized() {
                // this is the editor instance.
                me._preventTypingInLinks(this);
              }
            }
          }));
        }); // Init editor on footer links.

        block.querySelectorAll('footer .nav-link, header nav a.btn').forEach(function (link) {
          _this2.froalaEditor = new _this2.FroalaEditor('footer .nav-link, header nav a.btn', Object.assign(_this2.opts.editorOptions, {
            linkEditButtons: ['linkEdit', 'linkAdd', 'linkDestroy'],
            linkInsertButtons: [],
            events: {
              initialized: function initialized() {
                // this is the editor instance.
                me._preventTypingInLinks(this);
              }
            }
          }));
        });
      });
    }; // Disable editor.


    Editor.prototype.disable = function disable() {
      var _this3 = this;

      this.page.iframeBody.querySelectorAll('[data-block-type]').forEach(function (block) {
        block.querySelectorAll('[class*="col"]').forEach(function (col) {
          _this3.froalaEditor && _this3.froalaEditor.forEach(function (block) {
            if (block && !block.destrying) {
              block.destroy();
            }
          });
        }); // disable editor on header/footer links

        block.querySelectorAll('footer .nav-link, header .nav-link, header nav a.btn').forEach(function (col) {
          col.contentEditable = false;
        });
      });
    }; // Create custom buttons for links.


    Editor.prototype._defineLinkButtons = function _defineLinkButtons() {
      var that = this; // Remove link.

      this.FroalaEditor.DefineIcon('linkDestroy', {
        NAME: 'trash'
      });
      this.FroalaEditor.RegisterCommand('linkDestroy', {
        type: 'button',
        title: 'Remove Link',
        callback: function callback() {
          var $oel = this.$oel;
          this.destroy(); // Footer case.

          if ($oel.parents('.nav-item').length === 0) {
            $oel.remove();
          } else {
            $oel.parent().remove();
          }
        },
        refresh: function refresh($btn) {
          // Footer case.
          if (this.$oel.parent('.nav-item').length === 0) {
            $btn.get(0).classList.toggle('fr-disabled', this.$oel.siblings('.nav-link').length === 0);
          } else {
            $btn.get(0).classList.toggle('fr-disabled', this.$oel.parents('.nav-item').siblings('.nav-item').length === 0);
          }

          that.enable();
        }
      }); // Add link.

      this.FroalaEditor.DefineIcon('linkAdd', {
        NAME: 'plus'
      });
      this.FroalaEditor.RegisterCommand('linkAdd', {
        type: 'button',
        title: 'Add Link',
        callback: function callback() {
          var $oel = this.$oel; // Footer case.

          if ($oel.parents('.nav-item').length === 0) {
            $oel.after('<a class="nav-link" href="https://www.froala.com">Link</a>');
          } else {
            $oel.parent().after('<li class="nav-item"><a class="nav-link" href="https://www.froala.com">Link</a></li>');
          }
        }
      }); // Link button type.

      this.FroalaEditor.DefineIcon('linkButton', {
        NAME: 'star'
      });
      this.FroalaEditor.RegisterCommand('linkButton', {
        type: 'dropdown',
        title: 'Choose Style',
        options: {
          link: 'Link',
          button: 'Button',
          outline: 'Outline'
        },
        callback: function callback(cmd, val) {
          var link = this.link.get();

          if (val === 'link') {
            link.classList.add('nav-link');
            link.classList.remove('btn', 'btn-primary', 'btn-outline-primary');
          } else if (val === 'button') {
            link.classList.remove('nav-link', 'btn-outline-primary');
            link.classList.add('btn', 'btn-primary');
          } else if (val === 'outline') {
            link.classList.remove('nav-link', 'btn-primary');
            link.classList.add('btn', 'btn-outline-primary');
          }
        }
      });
    };

    return Editor;
  }(Module); // Block placeholders.


  var EmptyBlock = function (_M) {
    inherits(EmptyBlock, _M);

    function EmptyBlock() {
      classCallCheck(this, EmptyBlock);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    EmptyBlock.prototype.init = function init(info) {
      var _this2 = this; // Set info about the block.


      this.name = info.name;
      this.template = info.template;
      var el = this.doc.createElement('DIV');
      el.classList.add('fp-no-block', 'fp-no-' + this.name.toLowerCase());
      el.setAttribute('data-fp', 'true');
      el.innerHTML = this.template;
      this.el = el;
      this.events.on(el, 'click', function () {
        return _this2.click();
      });
      this.events.on(el, 'mouseenter', function () {
        return _this2._mouseEnter();
      });
    }; // Block on empty block.


    EmptyBlock.prototype.click = function click() {
      this.events.emit('emptyBlock.click', this);
    }; // Mouse enter on empty block.


    EmptyBlock.prototype._mouseEnter = function _mouseEnter() {
      this.page.blockToolbar.hide();
    };

    return EmptyBlock;
  }(Module);

  EmptyBlock.HEADER = {
    name: 'Headers',
    template: '<p>Header</p>'
  };
  EmptyBlock.FOOTER = {
    name: 'Footers',
    template: '<p>Footer</p>'
  };
  EmptyBlock.SECTION = {
    name: 'Sections',
    template: '<h1>Blank Website</h1><p>(click to add a design block)</p>'
  }; // Events.

  var Events = function () {
    function Events() {
      classCallCheck(this, Events); // DOM events.

      this.domEvents = {}; // Internal events.

      this.internalEvents = {};
    } // Set an ID for events.


    Events.prototype._id = function _id(el) {
      if (!this._ids) {
        this._ids = {};
      }

      if (this._ids[el]) {
        return this._ids[el];
      }

      this._ids[el] = Object.keys(this._ids).length + 1;
      return this._ids[el];
    }; // Bind DOM event.


    Events.prototype.on = function on(el, events, callback) {
      var _this = this;

      var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
      events.split(' ').forEach(function (event) {
        var id = _this._id(el);

        if (!_this.domEvents[id]) {
          _this.domEvents[id] = {};
        }

        if (!_this.domEvents[id][event]) {
          _this.domEvents[id][event] = [];
        }

        _this.domEvents[id][event].push({
          callback: callback,
          options: options
        });

        el.addEventListener(event, callback, options);
      });
    }; // Turn off DOM event.


    Events.prototype.off = function off(el, events) {
      var _this2 = this;

      events.split(' ').forEach(function (event) {
        var id = _this2._id(el);

        if (_this2.domEvents[id] && _this2.domEvents[id][event]) {
          _this2.domEvents[id][event].forEach(function (info) {
            el.removeEventListener(event, info.callback, info.options);
          });
        }
      });
    }; // Bind DOM event for a single time.


    Events.prototype.once = function once(el, events, callback) {
      var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
      options.once = true;
      this.on(el, events, callback, options);
    }; // Listen to internal event.


    Events.prototype.listen = function listen(events, callback) {
      var _this3 = this;

      events.split(' ').forEach(function (event) {
        if (!_this3.internalEvents[event]) {
          _this3.internalEvents[event] = [];
        }

        _this3.internalEvents[event].push(callback);
      });
    }; // Emit internal event.


    Events.prototype.emit = function emit(event) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      if (this.internalEvents[event]) {
        this.internalEvents[event].forEach(function (callback) {
          callback.apply(undefined, args);
        });
      }
    };

    return Events;
  }(); // Toolbar that is shown at the buttom of the page.


  var PageToolbar = function (_M) {
    inherits(PageToolbar, _M);

    function PageToolbar() {
      classCallCheck(this, PageToolbar);
      return possibleConstructorReturn(this, _M.apply(this, arguments));
    }

    PageToolbar.prototype.init = function init() {
      // DOM.
      this.el = this.doc.createElement('DIV');
      this.el.classList.add('fp-page-toolbar');
      Helpers.append(this.page.selector, this.el); // Buttons.

      this._addButtons(this.opts.pageLeftButtons, 'fp-lp-zone');

      this._addButtons(this.opts.pageCenterButtons, 'fp-cp-zone');

      this._addButtons(this.opts.pageRightButtons, 'fp-rp-zone');

      this.events.emit('button.refresh');
    }; // Build buttons.


    PageToolbar.prototype._addButtons = function _addButtons(buttonsList, id) {
      var _this2 = this;

      var zone = this.doc.createElement('DIV');
      zone.classList.add('fp-btn-zone');

      if (id) {
        zone.setAttribute('id', id);
      }

      buttonsList.forEach(function (groupEl) {
        if (Array.isArray(groupEl)) {
          var group = _this2.doc.createElement('DIV');

          group.classList.add('fp-btn-group');
          groupEl.forEach(function (btnName) {
            var btn = new Button(_this2.page, btnName);
            Helpers.append(group, btn.el);
          });
          Helpers.append(zone, group);
        } else {
          zone.innerHTML += groupEl;
        }
      });
      Helpers.append(this.el, zone);
    };

    return PageToolbar;
  }(Module);

  var Page = function () {
    function Page(selectorId, options$$1) {
      classCallCheck(this, Page); // Set options.

      this.opts = Object.assign({}, options, options$$1); // Get current DOM.

      this.doc = document;
      this.body = document.body; // Container.

      this.container = this.doc.createElement('DIV');
      this.container.classList.add('fp-container'); // render to selector element / body

      this.selector = this.doc.getElementById(selectorId) || this.body;
      Helpers.append(this.selector, this.container); // Actual editable zone.

      this.el = this.doc.createElement('DIV');
      this.el.classList.add('fp-element');
      Helpers.append(this.container, this.el); // Init events.

      this.events = new Events(); // Init designs panel.

      this.designsPanel = new DesignsPanel(this); // Render UI.

      this.render();
    } // Here we start rendering.


    Page.prototype.render = function render() {
      this._initIframe();

      this._initEmptyBlocks();

      this._initPageToolbar();

      this._initDropZone();

      this._initBlockToolbar();

      this._initEvents();

      this._initEditor(); // Refresh current blocks.


      this.refreshBlocks(); // Set default view as being the FULL one.

      this.setView(VIEWS.FULL);
    }; // Initialize Froala Editor.


    Page.prototype._initEditor = function _initEditor() {
      this.editor = new Editor(this);
    }; // Initialize the block toolbar.


    Page.prototype._initBlockToolbar = function _initBlockToolbar() {
      this.blockToolbar = new BlockToolbar(this);
    }; // Initialize the page toolbar.


    Page.prototype._initPageToolbar = function _initPageToolbar() {
      this.activeView = VIEWS.FULL;
      this.pageToolbar = new PageToolbar(this);

      if (this.selector !== this.body && !this.selector.style.height) {
        // set view height to 100% if there is no default value
        this.selector.style.height = '100%';
      }

      this.container.style.height = 'calc(100% - ' + this.pageToolbar.el.getBoundingClientRect().height + 'px)';
      this.data = new Data(this);
    }; // Build stylesheets for iframe.


    Page.prototype._buildStylesheets = function _buildStylesheets() {
      return this.opts.designStylesheets.map(function (x) {
        return '<link rel="stylesheet" href="' + x + '" />';
      }).join('');
    }; // Build stylesheets for iframe.


    Page.prototype._buildJavascripts = function _buildJavascripts() {
      return this.opts.designJavascripts.map(function (x) {
        return '<script src="' + x + '"></script>';
      }).join('');
    }; // Init the iframe.


    Page.prototype._initIframe = function _initIframe() {
      var iframe = document.createElement('IFRAME');
      this.iframe = iframe;
      iframe.setAttribute('src', 'about:blank'); // id is required for pages to whitelist license conditions for Editor

      iframe.setAttribute('id', 'froala-pages');
      Helpers.append(this.el, iframe); // Write iframe content.

      this.iframeDoc = iframe.contentWindow.document;
      this.iframeDoc.open();
      this.iframeDoc.write('<!DOCTYPE html>');
      this.iframeDoc.write('<html>\n                            <head>\n                              <meta name="viewport" content="width=device-width, initial-scale=1">\n                              ' + this._buildStylesheets() + '\n\n                              <style data-fp>' + this.opts.appStyle + '</style>\n                            </head>\n                            <body>\n                              ' + this._buildJavascripts() + '\n                            </body>\n                          </html>');
      this.iframeDoc.close(); // Add body and head.

      this.iframeBody = this.iframeDoc.body;
      this.iframeHead = this.iframeDoc.head;
    }; // Add empty block.


    Page.prototype._initEmptyBlocks = function _initEmptyBlocks() {
      this.emptySection = new EmptyBlock(this, EmptyBlock.SECTION);
      Helpers.append(this.iframeBody, this.emptySection.el);
    }; // Refresh the size of the iframe.


    Page.prototype.refreshIframeSize = function refreshIframeSize() {
      this.iframe.style.height = this.iframeBody.scrollHeight + 'px';
    }; // Refresh page blocks.


    Page.prototype.refreshBlocks = function refreshBlocks() {
      // Check if there are blocks.
      var noBlocks = !this.iframeBody.querySelector('[data-block-type]'); // Update CSS classes to reflect new block state.

      this.emptySection.el.classList.toggle('fp-visible', noBlocks);
      this.iframeBody.classList.toggle('no-block', noBlocks);
      this.container.classList.toggle('fp-no-block', noBlocks); // Change iframe size.

      this.refreshIframeSize(); // Display downloads button only if blocks are selected and has valid key

      var hideBtn = this.pageToolbar.ul ? this.pageToolbar.ul : noBlocks;
      Helpers.setDownloadBtnsStatus(this, noBlocks, hideBtn);
    }; // Refresh current view.


    Page.prototype.setView = function setView(view) {
      var _this = this;

      this.activeView = view; // Toggle CSS classes between views.

      this.body.classList.toggle('fp-add-view', this.activeView === VIEWS.ADD);
      this.body.classList.toggle('fp-full-view', this.activeView === VIEWS.FULL);
      this.iframeBody.classList.toggle('fp-add-view', this.activeView === VIEWS.ADD);
      this.iframeBody.classList.toggle('fp-full-view', this.activeView === VIEWS.FULL); // Toggle design blocks zone.

      this.designsPanel.setActiveBlock();

      if (this.activeView === VIEWS.ADD) {
        this.designsPanel.show();
      } else {
        this.designsPanel.hide();
      } // Make header and footer not draggable.


      Array.from(this.iframeBody.querySelectorAll('[data-block-type]:not(header):not(footer)')).forEach(function (el) {
        if (_this.activeView === VIEWS.ADD) {
          el.setAttribute('draggable', 'true');
        } else {
          el.removeAttribute('draggable');
        }
      }); // Refresh buttons.

      this.events.emit('button.refresh');
    }; // Initialize events.


    Page.prototype._initEvents = function _initEvents() {
      var _this2 = this; // Click on an empty block.


      this.events.listen('emptyBlock.click', function () {
        _this2.setView(VIEWS.ADD);
      }); // A design block was clicked.

      this.events.listen('designBlock.click', function (block) {
        return _this2.insertBlock(block);
      });
      this.events.listen('designBlock.willDrag', function (block) {
        _this2.draggingDesignBlock = block;
        _this2.dragImage = block.dragImage;

        if (_this2.dropPlaceholder) {
          _this2.dropPlaceholder.style.height = '';
        }
      });
    }; // Drag of a block ends.


    Page.prototype._dragend = function _dragend() {
      if (this.dropPlaceholder) {
        Helpers.remove(this.dropPlaceholder);
        this.dropPlaceholder.isVisible = false;
      }

      this.body.classList.remove('fp-dragging');
    }; // A block is dropped.


    Page.prototype._drop = function _drop() {
      if (this.activeView === VIEWS.ADD) {
        // Sorting blocks.
        if (this.draggingBlock && this.dropPlaceholder && this.dropPlaceholder.isVisible) {
          this.draggingBlock.classList.remove('fp-dragging');
          Helpers.after(this.dropPlaceholder, this.draggingBlock);
          Helpers.remove(this.dropPlaceholder);
          Helpers.remove(this.dragImage);
        } // Adding block.


        if (this.draggingDesignBlock && (this.dropPlaceholder && this.dropPlaceholder.isVisible || this.container.classList.contains('fp-no-block'))) {
          this.insertBlock(this.draggingDesignBlock, this.dropPlaceholder);
          Helpers.remove(this.dropPlaceholder);
          Helpers.remove(this.dragImage);
        }

        this.dropPlaceholder.isVisible = false;
        this.draggingDesignBlock = null;
        this.draggingBlock = null;
      }

      this.body.classList.remove('fp-dragging');
    }; // Drop placeholder.


    Page.prototype._initDropZone = function _initDropZone() {
      var _this3 = this;

      this.dropZone = this.doc.createElement('DIV');
      this.dropZone.classList.add('fp-drop-zone');
      this.dropZone.innerHTML = '<p>Drag & Drop a Design Block</p>\n                                <p>(or click one)</p>\n                                <div id="fp-drop-visual">\n                                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200.26 68.42">\n                                    <path d="M6.19,9.4Q4.9,7.87,3.62,6.3L5.18,5Q6.44,6.6,7.72,8.11Z"/>\n                                    <path d="M139.15,68.42q-2,0-4,0l0-2q4.09.08,8.16,0l0,2Q141.29,68.42,139.15,68.42ZM128,68.13c-2.75-.15-5.52-.35-8.22-.61l.19-2c2.68.25,5.42.45,8.14.6Zm22.65,0-.11-2c2.7-.14,5.44-.33,8.15-.58l.18,2C156.12,67.78,153.36,68,150.63,68.12ZM166,66.79l-.24-2c2.69-.32,5.41-.7,8.09-1.11l.31,2C171.48,66.09,168.73,66.47,166,66.79Zm-53.43-.08c-2.73-.36-5.47-.78-8.15-1.25l.35-2c2.64.47,5.36.88,8.06,1.24Zm68.69-2.25-.37-2c2.66-.49,5.35-1,8-1.62l.43,2C186.68,63.41,184,64,181.28,64.46Zm-83.92-.4c-2.68-.58-5.38-1.24-8-1.94l.52-1.93c2.6.69,5.27,1.34,7.93,1.92Zm-15-3.94c-2.63-.82-5.26-1.71-7.81-2.65l.69-1.88c2.53.93,5.12,1.81,7.72,2.62ZM67.87,54.84c-2.54-1.06-5.07-2.19-7.53-3.37l.86-1.8C63.63,50.83,66.13,52,68.64,53Zm-14-6.62c-2.41-1.29-4.82-2.66-7.17-4.07l1-1.71c2.32,1.4,4.71,2.75,7.08,4ZM40.62,40.27c-2.28-1.52-4.55-3.12-6.74-4.75l1.19-1.6c2.17,1.62,4.41,3.19,6.66,4.69ZM28.19,31.08c-2.11-1.73-4.22-3.54-6.25-5.38l1.34-1.48c2,1.82,4.09,3.61,6.18,5.31ZM16.69,20.75c-1.94-1.91-3.86-3.91-5.73-5.93l1.47-1.35c1.84,2,3.74,4,5.66,5.86Z"/>\n                                    <path d="M196.38,61.16l-.49-1.94q1.94-.49,3.85-1l.52,1.93Q198.33,60.67,196.38,61.16Z"/>\n                                    <polygon points="1.39 9.88 0 0 9.24 3.73 1.39 9.88"/>\n                                  </svg>\n                                </div>\n                                ';
      Helpers.append(this.container, this.dropZone); // Block is dragged over.

      this.events.on(this.dropZone, 'dragover dragenter', function (e) {
        e.preventDefault();

        _this3.dropZone.classList.add('fp-hover');
      }); // Drag ends.

      this.events.on(this.dropZone, 'dragleave dragend', function () {
        _this3.dropZone.classList.remove('fp-hover');
      }); // Block is dropped.

      this.events.on(this.dropZone, 'drop', function (e) {
        return _this3._drop(e);
      });
    }; // Make a design block draggable.


    Page.prototype._makeBlockDraggable = function _makeBlockDraggable(block) {
      var _this4 = this;

      var el = block.el;
      var isHeader = block.designBlock.blockType === DesignBlock.HEADER;
      var isFooter = block.designBlock.blockType === DesignBlock.FOOTER; // make it draggable

      if (!this.dropPlaceholder) {
        this.dropPlaceholder = this.doc.createElement('DIV');
        this.dropPlaceholder.classList.add('fp-drop-placeholder');
        this.events.on(this.iframeDoc, 'drop', function (e) {
          return _this4._drop(e);
        });
        this.events.on(this.doc, 'drop', function (e) {
          return _this4._drop(e);
        });
        this.events.on(this.iframeDoc, 'dragend', function (e) {
          return _this4._drop(e);
        });
        this.events.on(this.doc, 'dragend', function (e) {
          return _this4._drop(e);
        });
        this.events.on(this.iframeDoc, 'dragenter dragover', function (e) {
          return e.preventDefault();
        });
        this.events.on(this.doc, 'dragenter dragover', function (e) {
          return e.preventDefault();
        });
      }

      if (!isHeader && !isFooter) {
        el.setAttribute('draggable', 'true');
      } // Drag ends.


      this.events.on(el, 'dragover dragenter', function (e) {
        e.preventDefault(); // Drag can be used only in ADD mode.

        if (_this4.activeView === VIEWS.ADD) {
          var cy = e.clientY;
          var ey = Helpers.offset(el).top;
          var eh = Helpers.outerHeight(el); // Decide where to place drop placeholder based on the current mouse position.

          if (isHeader && cy > ey) {
            Helpers.after(el, _this4.dropPlaceholder);
          } else if (isFooter && cy > ey) {
            Helpers.before(el, _this4.dropPlaceholder);
          } else if (cy > ey + eh / 2) {
            Helpers.after(el, _this4.dropPlaceholder);
          } else {
            Helpers.before(el, _this4.dropPlaceholder);
          }

          _this4.dropPlaceholder.isVisible = true;

          _this4.refreshIframeSize();
        }
      }); // Drag starts.

      this.events.on(el, 'dragstart', function (e) {
        // Drag can be used only in ADD mode.
        if (_this4.activeView === VIEWS.ADD) {
          // Don't do it for footers and headers.
          if (isHeader || isFooter) {
            e.preventDefault();
            e.stopPropagation();
            return false;
          } // Let BODY element know we're dragging.


          _this4.body.classList.add('fp-dragging'); // Create a draggable placeholder.


          _this4.dragImage = _this4.doc.createElement('IMG');
          _this4.dragImage.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPhfDwAChwGA60e6kgAAAABJRU5ErkJggg==';
          _this4.dragImage.style.position = 'aboslute';
          Helpers.append(_this4.body, _this4.dragImage);
          e.dataTransfer.setDragImage(_this4.dragImage, 0, 0);
          e.dataTransfer.setData('text/html', null); // Use a timeout so image is loaded.

          setTimeout(function () {
            Helpers.after(el, _this4.dropPlaceholder);
            _this4.dropPlaceholder.style.height = Helpers.outerHeight(el, true) + 'px';
            _this4.dropPlaceholder.isVisible = true;
            el.classList.add('fp-dragging');
          });
          _this4.draggingBlock = el;
        }

        return true;
      });
    }; // Add a new block.


    Page.prototype.insertBlock = function insertBlock(designBlock, placeholder) {
      var _this5 = this;

      var block = new Block(this, designBlock); // Trigger scroll when images are loaded.

      Array.from(block.el.querySelectorAll('img')).forEach(function (img) {
        _this5.events.once(img, 'load', function () {
          return _this5.refreshIframeSize();
        });

        Helpers.scrollTo(block.el);
      }); // Add block to the body.

      if (designBlock.blockType === DesignBlock.HEADER) {
        // Replace existing header if there is one.
        var headerEl = this.iframeBody.querySelector('header');

        if (headerEl) {
          Helpers.remove(headerEl);
        }

        Helpers.prepend(this.iframeBody, block.el);
      } else if (designBlock.blockType === DesignBlock.FOOTER) {
        // Replace existing footer if there is one.
        var footerEl = this.iframeBody.querySelector('footer');

        if (footerEl) {
          Helpers.remove(footerEl);
        }

        Helpers.append(this.iframeBody, block.el);
      } else if (placeholder && placeholder.isVisible) {
        // Place new block after placeholder.
        Helpers.after(placeholder, block.el);
      } else {
        var _footerEl = this.iframeBody.querySelector('footer'); // Place new block at the end.


        if (_footerEl) {
          Helpers.before(_footerEl, block.el);
        } else {
          Helpers.append(this.iframeBody, block.el);
        }
      } // Refresh existing blocks.


      this.refreshBlocks(); // Make new block draggable.

      this._makeBlockDraggable(block); // Bind click event on the new block.


      this.events.on(block.el, 'click', function (e) {
        if (e.target && e.target.tagName === 'A') {
          e.preventDefault();
          return false;
        }

        if (e.target && e.target.tagName === 'A') {
          e.preventDefault();
          return false;
        }

        return true;
      }); // Emit block inserted event.

      this.events.emit('block.inserted', block);
    };

    Page.prototype.setBlockPlaceholder = function setBlockPlaceholder(block) {
      this._blockPlaceholder = block;
    }; // Get HTML for the HEAD.


    Page.prototype._getHeadHTML = function _getHeadHTML() {
      return Array.from(this.iframeDoc.querySelectorAll('head > *:not([data-fp])')).map(function (el) {
        return el.outerHTML;
      }).join('\n  ');
    }; // Get HTML for the BODY.


    Page.prototype._getBodyHTML = function _getBodyHTML() {
      return Array.from(this.iframeDoc.querySelectorAll('body > [data-block-type]')).map(function (el) {
        return el.outerHTML;
      }).join('\n') + '\n      ' + Array.from(this.iframeDoc.querySelectorAll('body > *:not([data-fp]):not([data-block-type])')).map(function (el) {
        return el.outerHTML;
      }).join('\n') + '\n    ';
    }; // Return promise which resolves get HTML for the page.


    Page.prototype.getHTML = function getHTML() {
      var _this6 = this;

      return new Promise(function (resolve, reject) {
        Data._handleHtml(_this6.data, resolve, reject);
      });
    }; // TODO: Get the blocks as a JSON structure instead of HTML.


    Page.prototype.getJSON = function getJSON() {} // {
    //   styles: [
    //     'link to first link tag.',
    //     'link to first second tag.'
    //   ],
    //   style_inline: 'inline CSS',
    //   scripts: [
    //     'link to first script tag.'
    //     'link to second script tag.'
    //   ],
    //   blocks: [
    //     'HTML for block 1.',
    //     'HTML for block 2.',
    //     'HTML for block 3.'
    //   ]
    // }
    // TODO: Load saved HTML.
    ;

    Page.prototype.loadHTML = function loadHTML() {} // Should load back saved HTML and rebuild existing page.
    // TODO: Load saved JSON.
    ;

    Page.prototype.loadJSON = function loadJSON() {// Should load back saved JSON and rebuild existing page.
    };

    return Page;
  }(); // Expose global classes.


  Page.Events = Events;
  Page.Helpers = Helpers;
  Page.RegisterDesignBlock = DesignBlock.Register;
  Page.Button = Button;
  return Page;
});

/***/ })

}]);