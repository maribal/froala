Vue.directive('visible', function(el, binding) {
    el.style.display = !!binding.value ? '' : 'none';
});
