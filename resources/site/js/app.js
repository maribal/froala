/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');


Vue.prototype.$api = axios.create({
    baseURL: "/json"
});

// require('froala-pages/js/froala_pages.min');
// require('froala-pages/js/pages_design_blocks_pkgd.min');


Vue.component('froala-component', require('./components/FroalasComponent.vue').default);




const app = new Vue({
    el: '#app',
});
