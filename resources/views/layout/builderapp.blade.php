<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>

{{--        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}

        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.5.0/js/all.js">

    <link href="https://cdn.jsdelivr.net/npm/froala-pages/css/froala_pages.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('admin/css/app.css') }}">

    @yield('css')

</head>
<body>

@yield('content')

</body>
{{--<script defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js"></script>--}}
<script src="/maribal/js/froala_pages.js"></script>
{{--<script src="/maribal/js/design_blocks/contents.js"></script>--}}
<script src="/maribal/js/design_blocks/call-to-action.js"></script>
<script src="/maribal/js/design_blocks/contacts.js"></script>
<script src="/maribal/js/design_blocks/contents.js"></script>
<script src="/maribal/js/design_blocks/custom.js"></script>
<script src="/maribal/js/design_blocks/features.js"></script>
<script src="/maribal/js/design_blocks/footers.js"></script>
<script src="/maribal/js/design_blocks/forms.js"></script>
{{--<script src="/maribal/js/design_blocks/headers.js"></script>--}}
<script src="/maribal/js/design_blocks/pricings.js"></script>
<script src="/maribal/js/design_blocks/teams.js"></script>
<script src="/maribal/js/design_blocks/testimonials.js"></script>

{{--<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-pages/js/pages_design_blocks_pkgd.min.js"></script>--}}
<script src="{{ asset('admin/js/app.js') }}"></script>

@yield('script')

</html>
