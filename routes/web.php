<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@dynamic');


Route::group(['prefix' => 'sudo'], function () {
    Voyager::routes();

    Route::get('/page/builder','BuilderController@build');
    Route::get('/page/edit','BuilderController@edit');



    Route::group(['prefix' => 'json'], function() {

        /* Permissions && Roles Checker */

        Route::post('permissions', 'RolesController@check');

        /* Pages Builder */
        Route::get('/page/{page}', 'BuilderController@index');


        Route::post('/page/{page}/builder', 'BuilderController@storeBuilder');
        Route::post('/page/{page}/editor', 'BuilderController@storeEditor');

        Route::post('/page/{page}/storeImage','BuilderController@storeImage');

    });


});


Route::get('page/{slug}', 'PagesController@dynamic');
